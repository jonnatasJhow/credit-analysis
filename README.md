CreditAnalysisApp
This project was generated with Spring Boot version 2.1.1. This is available on: https://credit-analysis.herokuapp.com/credit-analysis/api/

Entries available
 - /clients (POST/GET)
 - /risks (GET)
 - /credit-analysis (POST)

Development server
 - git clone git clone https://jonnatasJhow@bitbucket.org/jonnatasJhow/credit-analysis.git
 - mvn spring-boot:run
 - navigate to http://localhost:4200/
 
 
Spring Boot modules used
 - web
 - data jpa
 - h2 embedded