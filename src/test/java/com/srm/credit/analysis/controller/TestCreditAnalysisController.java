package com.srm.credit.analysis.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.srm.credit.analysis.AbstractControllerTest;
import com.srm.credit.analysis.model.CreditAnalysis;
import com.srm.credit.analysis.model.Risk;
import com.srm.credit.analysis.service.CreditAnalysisService;
import com.srm.credit.analysis.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(CreditAnalysisController.class)
public class TestCreditAnalysisController extends AbstractControllerTest {

	@MockBean
	CreditAnalysisService service;

	@Test
	public void shouldSaveACreditAnalysis() throws Exception {

		Risk riskB = Utils.createRisks().stream().filter(r -> r.getName().equals(Utils.RISK_B_NAME)).findFirst().get();

		CreditAnalysis creditAnalysis = new CreditAnalysis(Utils.createClients().get(0), 1000d, riskB);
		
		executePost(CreditAnalysisController.CREDIT_ANALYSIS_RESOURCE, mapper.writeValueAsString(creditAnalysis));
		
		BDDMockito.verify(service, VerificationModeFactory.times(1)).save(BDDMockito.any());
	}

}
