package com.srm.credit.analysis.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.ResultActions;

import com.srm.credit.analysis.AbstractControllerTest;
import com.srm.credit.analysis.dao.DAORisk;
import com.srm.credit.analysis.model.Risk;
import com.srm.credit.analysis.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(RiskController.class)
public class TestRiskController extends AbstractControllerTest {
	
	@MockBean
	DAORisk dao;
	
	@Test
	public void shouldFindAllRisks() throws Exception
	{
		List<Risk> risks = Utils.createRisks();
			
		given(dao.findAll()).willReturn(risks); 		

		ResultActions resultActions = executeGet(RiskController.RISK_RESOURCE);
		resultActions.andExpect(jsonPath("$", hasSize(risks.size())));
	}

}
