package com.srm.credit.analysis.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.List;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.ResultActions;

import com.srm.credit.analysis.AbstractControllerTest;
import com.srm.credit.analysis.dao.DAOClient;
import com.srm.credit.analysis.model.Client;
import com.srm.credit.analysis.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ClientController.class)
public class TestClientController extends AbstractControllerTest {
	@MockBean
	private DAOClient dao;

	private static final List<Client> CLIENTS = Utils.createClients();

	ArgumentCaptor<Client> captor = ArgumentCaptor.forClass(Client.class);
	
	@Test
	public void shouldFindAllClients() throws Exception {
		List<Client> clients = CLIENTS;

		given(dao.findAll()).willReturn(clients);

		ResultActions resultActions = executeGet(ClientController.CLIENT_RESOURCE);
		resultActions.andExpect(jsonPath("$", hasSize(clients.size())));
	}

	@Test
	public void souldFindClientById() throws Exception {
		Client client = CLIENTS.get(0);

		given(dao.findById(1l)).willReturn(Optional.of(client));

		ResultActions resultActions = executeGet(ClientController.CLIENT_RESOURCE + "/1");
		resultActions.andExpect(jsonPath("$.name", Matchers.is(client.getName())));
	}

	@Test
	public void shouldClientFindByName() throws Exception {
		List<Client> clients = CLIENTS;

		given(dao.findByNameStartsWithIgnoreCaseOrderByName(BDDMockito.anyString())).willReturn(clients);

		ResultActions resultActions = executeGet(ClientController.CLIENT_RESOURCE + "/?name=Teste 1");
		resultActions.andExpect(jsonPath("$", hasSize(clients.size())));
	}

	@Test
	public void shouldSaveClient() throws Exception {
		Client client = CLIENTS.get(0);

		executePost(ClientController.CLIENT_RESOURCE, mapper.writeValueAsString(client));

		BDDMockito.verify(dao, VerificationModeFactory.times(1)).save(captor.capture());
		
		assertEquals(client.getName(), captor.getValue().getName());
	}
}
