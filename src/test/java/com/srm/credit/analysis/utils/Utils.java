package com.srm.credit.analysis.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.srm.credit.analysis.model.Client;
import com.srm.credit.analysis.model.CreditAnalysis;
import com.srm.credit.analysis.model.Risk;

public class Utils {

	private static final String CLIENT_1_NAME = "Test 1";

	private static final String RISK_A_NAME = "A";
	public static final String RISK_B_NAME = "B";
	private static final String RISK_C_NAME = "C";

	public static List<Client> createClients() {
		return Arrays.asList(new Client(CLIENT_1_NAME));
	}

	public static List<Risk> createRisks() {
		return Arrays.asList(new Risk(RISK_A_NAME, BigDecimal.ZERO.doubleValue()), new Risk(RISK_B_NAME, 10d),
				new Risk(RISK_C_NAME, 20d));
	}

	public static List<CreditAnalysis> createCreditAnalysis() {
		Client client = createClients().get(0);
		Risk risk = createRisks().get(0);
		
		return Arrays.asList(new CreditAnalysis(client, 1000d, risk));
	}
}
