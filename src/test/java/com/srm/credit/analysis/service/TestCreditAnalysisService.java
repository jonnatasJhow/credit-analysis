package com.srm.credit.analysis.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.srm.credit.analysis.dao.DAOCreditAnalysis;
import com.srm.credit.analysis.model.CreditAnalysis;
import com.srm.credit.analysis.utils.Utils;

@RunWith(MockitoJUnitRunner.class)
public class TestCreditAnalysisService {

	@Mock
	DAOCreditAnalysis dao;
	
	@InjectMocks
	CreditAnalysisService service;
	
	ArgumentCaptor<CreditAnalysis> captor = ArgumentCaptor.forClass(CreditAnalysis.class);

	@Test
	public void shouldSaveCreditAnalysis()
	{
		CreditAnalysis creditAnalysis = Utils.createCreditAnalysis().get(0);
		
		service.save(creditAnalysis);
		
		Mockito.verify(dao, Mockito.times(1)).save(captor.capture());
		
		assertEquals(creditAnalysis.getRisk().getInterestRate(), captor.getValue().getInterestRate());
	}
}

