package com.srm.credit.analysis;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractControllerTest {

	@Autowired
	MockMvc mvc;

	protected final ObjectMapper mapper = new ObjectMapper();
	
	protected ResultActions executeGet(String pResource) throws Exception {

		return mvc.perform(get(pResource).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	protected ResultActions executePost(String pResource, String pContent) throws Exception {

		return mvc.perform(post(pResource).content(pContent).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
}
