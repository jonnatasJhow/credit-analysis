insert into risk(name, interest_rate)
values('A', 0);

insert into risk(name, interest_rate)
values('B', 10);

insert into risk(name, interest_rate)
values('C', 10);

insert into client(name)
values('Client 1');

insert into client(name)
values('Client 2');

insert into client(name)
values('Client 3');

insert into credit_analysis(client_id, credit_limit, risk_id, interest_rate)
values((select id from client where name = 'Client 1'), 1000, (select id from risk where name = 'A'), 0);