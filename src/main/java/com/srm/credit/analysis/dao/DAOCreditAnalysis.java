package com.srm.credit.analysis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.srm.credit.analysis.model.CreditAnalysis;

/**
 * Repository manipulation to {@link CreditAnalysis}.
 */
public interface DAOCreditAnalysis extends JpaRepository<CreditAnalysis, Long> {

}
