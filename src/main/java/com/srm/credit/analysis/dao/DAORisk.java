package com.srm.credit.analysis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.srm.credit.analysis.model.Risk;

/**
 * Repository manipulation to {@link Risk}.
 */
public interface DAORisk extends JpaRepository<Risk, Long> {
}
