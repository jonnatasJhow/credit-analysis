package com.srm.credit.analysis.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.srm.credit.analysis.model.Client;

/**
 * Repository manipulation to {@link Client}.
 */
public interface DAOClient extends JpaRepository<Client, Long> {

	/**
	 * Finds all clients that the name starts with the name passed. This ignores case when comparing character.
	 * 
	 * @param pName the name to search
	 * @return a {@link List} of {@link Client}
	 */
	List<Client> findByNameStartsWithIgnoreCaseOrderByName(String pName);
}
