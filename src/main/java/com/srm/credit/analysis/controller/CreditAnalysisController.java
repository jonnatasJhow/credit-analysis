package com.srm.credit.analysis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.srm.credit.analysis.model.CreditAnalysis;
import com.srm.credit.analysis.service.CreditAnalysisService;

/**
 * Controller manipulation to {@link CreditAnalysis}.
 */
@RestController
@RequestMapping("credit-analysis")
public class CreditAnalysisController {

	public static final String CREDIT_ANALYSIS_RESOURCE = "/credit-analysis";
	
	@Autowired
	private CreditAnalysisService service;
	
	@PostMapping
	public CreditAnalysis save(@RequestBody CreditAnalysis pCreditAnalysis)
	{
		return service.save(pCreditAnalysis);
	}
}
