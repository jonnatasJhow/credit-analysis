package com.srm.credit.analysis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.srm.credit.analysis.dao.DAORisk;
import com.srm.credit.analysis.model.Risk;

/**
 * Controller manipulation to {@link Risk}.
 */
@RestController
@RequestMapping("risks")
public class RiskController {
	
	public static final String RISK_RESOURCE = "/risks";
	
	@Autowired
	private DAORisk dao;
	
	@GetMapping
	public List<Risk> findAll()
	{
		return dao.findAll();
	}
}
