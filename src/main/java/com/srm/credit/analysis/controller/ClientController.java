package com.srm.credit.analysis.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.srm.credit.analysis.dao.DAOClient;
import com.srm.credit.analysis.model.Client;

/**
 * Controller manipulation to {@link Client}.
 */
@RestController
@RequestMapping("clients")
public class ClientController {

	public static final String CLIENT_RESOURCE = "/clients";
	
	@Autowired
	private DAOClient dao;
	
	@GetMapping
	public List<Client> findAll()
	{
		return dao.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Client> findById(@PathVariable("id") Long pId)
	{
		return dao.findById(pId);
	}
	
	@GetMapping("/")
	public List<Client> findByName(@RequestParam("name") String pName)
	{
		return dao.findByNameStartsWithIgnoreCaseOrderByName(pName);
	}
	
	@PostMapping
	public Client save(@Valid @RequestBody Client pClient)
	{
		return dao.save(pClient);
	}
}
