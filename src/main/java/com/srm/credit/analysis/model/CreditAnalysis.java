package com.srm.credit.analysis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class that describes the credit analysis model.
 */
@Entity
@Table(name = "credit_analysis")
public class CreditAnalysis implements Serializable, Comparator<CreditAnalysis> {

	private static final long serialVersionUID = 3107322406540973552L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "client_id", nullable = false)
	private Client client;

	@Column(name = "credit_limit", nullable = false)
	private Double creditLimit;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "risk_id", nullable = false)
	private Risk risk;

	@Column(name = "interest_rate", nullable = false)
	private Double interestRate;
	
	@Column(name="creation_date", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private LocalDateTime creationDate;

	/**
	 * Constructor to {@link Client}.
	 */
	public CreditAnalysis() {
	}

	/**
	 * Constructor to {@link Client}.
	 * 
	 * @param pClient
	 *            the {@link Client}
	 * @param pCreditLimit
	 *            the limit of credit
	 * @param pRisk
	 *            the {@link Risk}
	 * @param pInterestRate
	 *            the rate
	 */
	public CreditAnalysis(Client pClient, Double pCreditLimit, Risk pRisk, Double pInterestRate) {

		this.client = pClient;
		this.creditLimit = pCreditLimit;
		this.risk = pRisk;
		this.interestRate = pInterestRate;
	}

	/**
	 * Constructor to {@link Client}.
	 * 
	 * @param pClient
	 *            the {@link Client}
	 * @param pCreditLimit
	 *            the limit of credit
	 * @param pRisk
	 *            the {@link Risk}
	 */
	public CreditAnalysis(Client pClient, Double pCreditLimit, Risk pRisk) {

		this(pClient, pCreditLimit, pRisk, BigDecimal.ZERO.doubleValue());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Client getClient() {
		return client;
	}

	@JsonProperty
	public void setClient(Client client) {
		this.client = client;
	}

	public Double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Risk getRisk() {
		return risk;
	}

	public void setRisk(Risk risk) {
		this.risk = risk;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public String getCreationDate() {
		return creationDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}

	@JsonProperty("creationDate")
	public LocalDateTime getCreationDateString() {
		return creationDate;
	}

	@JsonIgnore
	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int compare(CreditAnalysis o1, CreditAnalysis o2) {
		return o1.getCreationDate().compareTo(o2.getCreationDate());
	}
}