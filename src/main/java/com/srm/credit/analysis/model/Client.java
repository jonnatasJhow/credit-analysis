package com.srm.credit.analysis.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

/**
 * Class that describes the client model.
 */
@Entity
public class Client implements Serializable {

	private static final long serialVersionUID = 6664373009344162676L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Name is missing.")
	@Column(nullable = false)
	private String name;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "client")
	private List<CreditAnalysis> creditAnalysis;

	/**
	 * Constructor to {@link Client}.
	 */
	public Client() {
	}

	/**
	 * Constructor to {@link Client}.
	 * 
	 * @param pName
	 *            the client name
	 */
	public Client(String pName) {
		this.name = pName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CreditAnalysis> getCreditAnalysis() {
		if (creditAnalysis != null) {
			Collections.reverse(creditAnalysis);
		}
		return creditAnalysis;
	}

	public void setCreditAnalysis(List<CreditAnalysis> creditAnalysis) {
		this.creditAnalysis = creditAnalysis;
	}
}
