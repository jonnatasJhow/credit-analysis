package com.srm.credit.analysis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Class that describes the risk model.
 */
@Entity
public class Risk implements Serializable {

	private static final long serialVersionUID = -1542496651778545294L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false)
	private String name;
	
	@Column(name="interest_rate", nullable=false)
	private Double interestRate;

	public Risk()
	{
	}
	
	public Risk(String pName, Double pInterestRate) {
		
		name = pName;
		interestRate = pInterestRate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setValue(Double value) {
		this.interestRate = value;
	}
}