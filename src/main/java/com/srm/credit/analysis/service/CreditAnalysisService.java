package com.srm.credit.analysis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srm.credit.analysis.dao.DAOCreditAnalysis;
import com.srm.credit.analysis.model.CreditAnalysis;

/**
 * Business logic manipulation to {@link CreditAnalysis}/
 */
@Service
public class CreditAnalysisService {

	@Autowired
	private DAOCreditAnalysis dao;

	/**
	 * Persists the {@link CreditAnalysis}.
	 * 
	 * @param pCreditAnalysis the {@link CreditAnalysis} to persist
	 * @return the {@link CreditAnalysis}
	 */
	public CreditAnalysis save(CreditAnalysis pCreditAnalysis) {
		pCreditAnalysis.setInterestRate(pCreditAnalysis.getRisk().getInterestRate());
		return dao.save(pCreditAnalysis);
	}
}